package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Cours
 *
 */
@Entity
public class Cours implements Serializable {

	@Id
	private int idCours;
	private Date date;
	private String libelle;
	private String description;
	private int duree;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "cours", cascade = CascadeType.ALL)
	private List<EtudiantCours> listeEtudiants;

	public List<EtudiantCours> getListeEtudiants() {
		return listeEtudiants;
	}

	public void setListeEtudiants(List<EtudiantCours> listeEtudiants) {
		this.listeEtudiants = listeEtudiants;
	}

	public Cours() {
		super();
	}

	public int getIdCours() {
		return this.idCours;
	}

	public void setIdCours(int idCours) {
		this.idCours = idCours;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDuree() {
		return this.duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

}
