package domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class EtudiantCoursId implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idEtudiant;
	private int idCours;
	public int getIdEtudiant() {
		return idEtudiant;
	}
	public void setIdEtudiant(int idEtudiant) {
		this.idEtudiant = idEtudiant;
	}
	public int getIdCours() {
		return idCours;
	}
	public void setIdCours(int idCours) {
		this.idCours = idCours;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCours;
		result = prime * result + idEtudiant;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EtudiantCoursId other = (EtudiantCoursId) obj;
		if (idCours != other.idCours)
			return false;
		if (idEtudiant != other.idEtudiant)
			return false;
		return true;
	}
	
}
