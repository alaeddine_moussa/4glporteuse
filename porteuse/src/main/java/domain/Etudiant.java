package domain;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Etudiant
 * 
 * @author Aladin
 */
@Entity
public class Etudiant implements Serializable {

	@Id
	private int idEtudiant;
	private Date dateNaissance;
	private String photo;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "etudiant", cascade = CascadeType.ALL)
	private List<EtudiantCours> listeCours;

	public List<EtudiantCours> getListeCours() {
		return listeCours;
	}

	public void setListeCours(List<EtudiantCours> listeCours) {
		this.listeCours = listeCours;
	}

	public Etudiant() {
		super();
	}

	public int getIdEtudiant() {
		return this.idEtudiant;
	}

	public void setIdEtudiant(int idEtudiant) {
		this.idEtudiant = idEtudiant;
	}

	public Date getDateNaissance() {
		return this.dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

}
