package domain;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: EtudiantCours
 *
 */
@Entity
public class EtudiantCours implements Serializable {

	@EmbeddedId
	private EtudiantCoursId etudiantCoursId;
	private boolean absence;
	private String motif;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "idEtudiant", insertable = false, updatable = false)
	private Etudiant etudiant;
	@ManyToOne
	@JoinColumn(name = "idCours", insertable = false, updatable = false)
	private Cours cours;

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public EtudiantCours() {
		super();
	}

	public boolean getAbsence() {
		return this.absence;
	}

	public void setAbsence(boolean absence) {
		this.absence = absence;
	}

	public String getMotif() {
		return this.motif;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

}
